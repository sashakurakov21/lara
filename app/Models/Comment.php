<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{   //optional
    use HasFactory;

    protected $fillable = [
        'comment_content'
    ];

    public function post(){
        return $this->belongsTo(Post::class);
    }
}
