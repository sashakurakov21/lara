<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PageController;
use Faker\Provider\ar_EG\Color;
use Symfony\Component\Routing\Route as RoutingRoute;

use App\Http\Controllers\Page1Controller;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['register' => false]);

Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::middleware(['throttle:page1'])->group(function () {
    Route::get('/page1', [Page1Controller::class, 'index']);
});

// Route::prefix('/')->middleware('auth')->group(function() {


// });

